//
// Created by fazasaffanah on 26/12/2019.
//

#include <jni.h>
#include <cstdlib>

extern "C" JNIEXPORT jint JNICALL
Java_id_ac_ui_cs_mobileprogramming_nurulfazasaffanah_kutubuku_view_ui_BookListFragment_recommend(
        JNIEnv *env,
        jobject, jint number) {

    int result = rand() % number;

    return result;
}
