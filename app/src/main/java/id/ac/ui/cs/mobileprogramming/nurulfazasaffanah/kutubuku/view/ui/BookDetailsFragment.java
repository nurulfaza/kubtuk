package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.util.Objects;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.R;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.databinding.BookDetailsFragmentBinding;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.DownloadService;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Book;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.viewmodel.BookDetailsViewModel;

import static android.content.Context.NOTIFICATION_SERVICE;

public class BookDetailsFragment extends Fragment implements View.OnClickListener {
    private static final String BOOK_ISBN = "isbn13";
    private static final int PERMISSION_REQUEST_CODE = 200;
    private BookDetailsFragmentBinding binding;
    private Book currentBook;
    private ServiceResultReceiver serviceResultReceiver;

    public static int NOTIFICATION_ID = 27;
    public static final String NOTIFICATION_CHANNEL_ID = "channel_id";
    public static final String CHANNEL_NAME = "Notification Channel";
    NotificationChannel notificationChannel;


    public static BookDetailsFragment newInstance() {
        return new BookDetailsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.book_details_fragment, null, false);
        View view = binding.getRoot();
        Button borrowButton = view.findViewById(R.id.borrowButton);
        Button downloadButton = view.findViewById(R.id.downloadImageButton);
        borrowButton.setOnClickListener(this);
        downloadButton.setOnClickListener(this);

        initNotificationChannel();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        BookDetailsViewModel.Factory factory = new BookDetailsViewModel.Factory(
                getActivity().getApplication(), getArguments().getString(BOOK_ISBN));

        final BookDetailsViewModel viewModel = ViewModelProviders.of(this, factory)
                .get(BookDetailsViewModel.class);

        binding.setBookViewModel(viewModel);
        binding.setIsLoading(true);

        observeViewModel(viewModel);

        //book = viewModel.getBookObservable().getValue();
        //bookDetailsViewModel = ViewModelProviders.of(this).get(BookDetailsViewModel.class);

    }

    private void observeViewModel(final BookDetailsViewModel viewModel) {
        viewModel.getBookObservable().observe(getActivity(), new Observer<Book>() {
            @Override
            public void onChanged(Book book) {
                if (book != null) {
                    binding.setIsLoading(false);
                    viewModel.setBook(book);
                    currentBook = viewModel.getBookObservable().getValue();
                }
            }
        });
    }

    public static BookDetailsFragment forBook(String isbn13) {
        BookDetailsFragment fragment = new BookDetailsFragment();
        Bundle args = new Bundle();

        args.putString(BOOK_ISBN, isbn13);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.borrowButton):
                if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                    ((MainActivity)getActivity()).showFillData(currentBook);
                }
                break;
            case (R.id.downloadImageButton):
                if (ContextCompat.checkSelfPermission(getContext(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSION_REQUEST_CODE);
                }
                else {
                    downloadImage();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                downloadImage();
            }
            else {
                if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                    ((MainActivity)getActivity()).showAccessDenied();
                }
            }
        }
    }

    private void downloadImage() {
        serviceResultReceiver = new ServiceResultReceiver(new Handler());
        String urlImage = currentBook.getImage();
        String fileName = currentBook.getTitle().replaceAll("\\s+","") + ".png";

        // Progress bar
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity(), NOTIFICATION_CHANNEL_ID);
        builder.setSmallIcon(R.drawable.kb_logo);
        builder.setContentTitle("Downloading " + fileName);
        builder.setProgress(0, 0, true);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getActivity());
        notificationManagerCompat.notify(NOTIFICATION_ID, builder.build());

        Intent intent = new Intent(getActivity(), DownloadService.class);
        intent.putExtra("receiver", serviceResultReceiver);
        intent.putExtra("url", urlImage);
        intent.putExtra("filename", fileName);
        intent.putExtra("notification", Integer.toString(NOTIFICATION_ID));
        getActivity().startService(intent);
        Toast.makeText(getActivity(), "Download in progress...", Toast.LENGTH_SHORT).show();
        NOTIFICATION_ID++;
    }

    private void initNotificationChannel() {
        int importance = NotificationManager.IMPORTANCE_DEFAULT;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, CHANNEL_NAME, importance);
            notificationChannel.enableLights(true);
            notificationChannel.enableVibration(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.setVibrationPattern(new long[] { 500, 500, 500, 500, 500 });
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        }
        NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(notificationChannel);
    }

    private void displayNotification(String filename, String path, int notificationID) {
        Intent intent =  getOpenFileIntent(getContext().getApplicationContext(), path);
        PendingIntent contentIntent = PendingIntent.getActivity(getActivity(), 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                getActivity(), NOTIFICATION_CHANNEL_ID);
        builder.setContentIntent(contentIntent);
        builder.setContentTitle(filename);
        builder.setContentText("File Downloaded. Tap to open file");
        builder.setSmallIcon(R.drawable.kb_logo);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.kb_logo));
        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManagerCompat notificationManagerCompat =
                NotificationManagerCompat.from(getActivity());
        notificationManagerCompat.notify(notificationID, notification);
    }

    public static Intent getOpenFileIntent(Context context, String path) {
        File file = new File(path);
        Intent intent = new Intent(Intent.ACTION_VIEW);

        Uri apkURI = FileProvider.getUriForFile(
                context,
                context.getApplicationContext()
                        .getPackageName() + ".provider", file);

        intent.setDataAndType(apkURI, "image/");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        return intent;
    }


    class ServiceResultReceiver extends ResultReceiver {
        public ServiceResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            switch (resultCode) {
                case DownloadService
                        .DOWNLOAD_SUCCESS:
                    String filePath = resultData.getString(DownloadService.FILEPATH);
                    String fileName = resultData.getString(DownloadService.FILENAME);
                    Toast.makeText(getActivity(), "Download Success.", Toast.LENGTH_SHORT).show();
                    int notificationID = Integer.parseInt(Objects.requireNonNull(resultData.getString(DownloadService.NOTIFICATION)));
                    displayNotification(fileName, filePath, notificationID);
                    break;

            }
            super.onReceiveResult(resultCode, resultData);
        }
    }
}
