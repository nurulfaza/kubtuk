package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.R;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Borrow;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.adapter.BorrowListAdapter;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.viewmodel.BorrowViewModel;

public class BorrowListFragment extends Fragment {
    private BorrowViewModel borrowViewModel;
    private BorrowListAdapter borrowListAdapter;
    private Context context;

    public static BorrowListFragment newInstance() {
        return new BorrowListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.borrow_list_fragment, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        borrowListAdapter = new BorrowListAdapter();
        recyclerView.setAdapter(borrowListAdapter);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final BorrowViewModel borrowViewModel = ViewModelProviders.of(this).get(BorrowViewModel.class);
        borrowViewModel.getBorrowList().observe(getActivity(), new Observer<List<Borrow>>() {
            @Override
            public void onChanged(List<Borrow> borrowList) {
                borrowListAdapter.setBorrows(borrowList);
            }
        });
    }

}
