package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Book;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppRepository {
    private ITBookStoreApi itBookStoreApi;
    private static AppRepository appRepository;

    public AppRepository() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(itBookStoreApi.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        itBookStoreApi = retrofit.create(ITBookStoreApi.class);

    }

    public synchronized static AppRepository getInstance() {
        if (appRepository == null) {
            appRepository = new AppRepository();
        }
        return appRepository;
    }

    public LiveData<BookList> getBookList() {
        final MutableLiveData<BookList> data = new
                MutableLiveData<>();

        itBookStoreApi.getBookList()
                .enqueue(new Callback<BookList>() {
                    @Override
                    public void onResponse(Call<BookList> call, Response<BookList> response) {
                        data.setValue(response.body());

                    }

                    @Override
                    public void onFailure(Call<BookList> call, Throwable t) {

                    }
                });
        return data;
    }

    public LiveData<Book> getBookDetails(String isbn13) {
        final MutableLiveData<Book> data = new MutableLiveData<>();
        itBookStoreApi.getBookDetails(isbn13)
                .enqueue(new Callback<Book>() {
                    @Override
                    public void onResponse(Call<Book> call, Response<Book> response) {
                        //simulateDelay();
                        data.setValue(response.body());
                    }

                    @Override
                    public void onFailure(Call<Book> call, Throwable t) {
                        data.setValue(null);
                    }
                });
        return data;
    }

}
