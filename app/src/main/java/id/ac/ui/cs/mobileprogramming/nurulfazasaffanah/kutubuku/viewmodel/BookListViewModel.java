package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.repository.AppRepository;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.repository.BookList;

public class BookListViewModel extends AndroidViewModel {
    private final LiveData<BookList> bookListObservable;

    public BookListViewModel(Application application) {
        super(application);

        bookListObservable = AppRepository.getInstance().getBookList();
    }

    public LiveData<BookList> getBookListObservable() {
        return bookListObservable;
    }

}
