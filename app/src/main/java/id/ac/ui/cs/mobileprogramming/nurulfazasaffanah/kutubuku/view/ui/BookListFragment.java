package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.callback.BookClickCallback;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.repository.BookList;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.R;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.databinding.BookListFragmentBinding;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Book;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.adapter.BookListAdapter;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.viewmodel.BookListViewModel;

public class BookListFragment extends Fragment {

    static {
        System.loadLibrary("native-lib");
    }

    public static final String TAG = "BookListFragment";
    //private BookListViewModel bookListViewModel;
    private BookListAdapter bookListAdapter;
    private BookListFragmentBinding binding;
    private Book recommendedBook;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        //View view = inflater.inflate(R.layout.book_list_fragment, container, false);
        //openGLView = view.findViewById(R.id.open_gl);

        binding = DataBindingUtil.inflate(inflater, R.layout.book_list_fragment, container, false);
        //RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        bookListAdapter = new BookListAdapter(bookClickCallback);
        binding.recyclerView.setAdapter(bookListAdapter);
        binding.setIsLoading(true);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        final BookListViewModel bookListViewModel = ViewModelProviders.of(this).get(BookListViewModel.class);
        bookListViewModel.getBookListObservable().observe(getActivity(), new Observer<BookList>() {
            @Override
            public void onChanged(BookList bookList) {
                if (bookList != null) {
                    binding.setIsLoading(false);
                    bookListAdapter.setBooks(bookList);

                    //get a recommended book
                    int recommendedBookInd = recommend(bookList.getBooks().size());
                    recommendedBook = bookList.getBooks().get(recommendedBookInd);
                    binding.setRecBook(recommendedBook);
                    binding.setCallback(bookClickCallback);
                }
            }
        });


    }

    private final BookClickCallback bookClickCallback = new BookClickCallback() {
        @Override
        public void onClick(Book book) {
            if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                ((MainActivity)getActivity()).showDetails(book);
            }
        }
    };


    public native int recommend(int number);

}
