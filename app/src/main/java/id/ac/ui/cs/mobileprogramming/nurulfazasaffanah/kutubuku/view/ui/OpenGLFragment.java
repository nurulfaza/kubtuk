package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.R;

public class OpenGLFragment extends Fragment {
    public static OpenGLFragment newInstance() {
        OpenGLFragment fragment = new OpenGLFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //required for back button to work
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.opengl_fragment, container, false);
    }
}
