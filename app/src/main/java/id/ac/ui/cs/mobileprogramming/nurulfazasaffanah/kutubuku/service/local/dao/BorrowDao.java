package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Borrow;

@Dao
public interface BorrowDao {

    @Insert
    void insert(Borrow borrow);

    @Query("SELECT * FROM borrow_table")
    LiveData<List<Borrow>> getAllBorrows();
}
