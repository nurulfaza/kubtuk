package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.User;

@Dao
public interface UserDao {
    @Insert
    void insert(User user);

    @Query("DELETE FROM user_table")
    void deleteAllUser();
}
