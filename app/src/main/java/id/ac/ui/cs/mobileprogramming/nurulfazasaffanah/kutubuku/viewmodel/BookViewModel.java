package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.AppRoomDatabase;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Book;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.dao.BookDao;

public class BookViewModel extends AndroidViewModel {
    BookDao bookDao;

    public BookViewModel(@NonNull Application application) {
        super(application);
        bookDao = AppRoomDatabase.getDatabase(application).bookDao();
    }

    public void insert(Book book) {
        bookDao.insert(book);
    }
}
