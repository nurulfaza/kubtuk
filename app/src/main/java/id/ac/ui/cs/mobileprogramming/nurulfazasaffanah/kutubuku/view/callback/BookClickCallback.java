package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.callback;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Book;

public interface BookClickCallback {
    void onClick(Book book);
}
