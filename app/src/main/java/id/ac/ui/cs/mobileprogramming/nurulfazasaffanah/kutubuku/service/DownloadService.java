package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;

import androidx.annotation.Nullable;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadService extends IntentService {
    public static final int DOWNLOAD_SUCCESS = 11;
    public static final String URL = "url";
    public static final String FILENAME = "filename";
    public static final String FILEPATH = "filepath";
    public static final String NOTIFICATION = "notification";
    public static final String RECEIVER = "receiver";

    public DownloadService() {
        super(DownloadService.class.getName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String urlPath = intent.getStringExtra(URL);
        String fileName = intent.getStringExtra(FILENAME);
        String notificationID = intent.getStringExtra(NOTIFICATION);
        String outputFileName = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + fileName;
        final ResultReceiver receiver = intent.getParcelableExtra(RECEIVER);
        Bundle bundle = new Bundle();

        try {
            URL url = new URL(urlPath);
            URLConnection urlConnection = url.openConnection();
            urlConnection.connect();

            InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
            OutputStream outputStream = new FileOutputStream(outputFileName);

            byte buffer[] = new byte[1024];
            int count;
            while ((count = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, count);
            }
            outputStream.flush();
            outputStream.close();
            inputStream.close();

            bundle.putString(FILENAME, fileName);
            bundle.putString(FILEPATH, outputFileName);
            bundle.putString(NOTIFICATION, notificationID);
            receiver.send(DOWNLOAD_SUCCESS, bundle);

        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
