package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.renderscript.Float3;
import android.renderscript.Matrix4f;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.R;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui.glkit.ShaderProgram;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui.glkit.ShaderUtils;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui.glkit.TextureUtils;

public class OpenGLRenderer implements GLSurfaceView.Renderer {
    private static final float ONE_SEC = 1000.0f;

    private Context context;
    private MaskedSquare logo;

    private long lastTimeMillis = 0L;

    public OpenGLRenderer(Context context) {
        this.context = context;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        ShaderProgram shader = new ShaderProgram(
                ShaderUtils.readShaderFileFromRawResource(context, R.raw.simple_vertex_shader),
                ShaderUtils.readShaderFileFromRawResource(context, R.raw.simple_fragment_shader)
        );

        int logoTexture = TextureUtils.loadTexture(context, R.drawable.kb_logo);

        logo = new MaskedSquare(shader);

        logo.setPosition(new Float3(0.0f, 0.0f, 0.1f));
        logo.setTexture(logoTexture);

        lastTimeMillis = System.currentTimeMillis();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES20.glViewport(0, 0, width, height);

        Matrix4f prespective = new Matrix4f();
        prespective.loadPerspective(85.0f, (float)width / (float)height, 1.0f, -150.0f);
        if (logo != null) {
            logo.setProjection(prespective);
        }
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        GLES20.glClearColor(0.85f, 0.96f, 0.95f, 1.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glEnable(GLES20.GL_CULL_FACE);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        long currentTimeMillis = System.currentTimeMillis();
        updateWithDelta(currentTimeMillis - lastTimeMillis);
        lastTimeMillis = currentTimeMillis;
    }

    public void updateWithDelta(long dt) {

        Matrix4f camera = new Matrix4f();
        camera.translate(0.0f, 0.0f, -5.0f);

        logo.setCamera(camera);

        final float secsPerMove = 5.0f * ONE_SEC;
        float movement = (float) (Math.sin(System.currentTimeMillis() * 2 * Math.PI / secsPerMove));
        logo.setPosition(new Float3(movement, logo.position.y, logo.position.z));
        logo.draw(dt);
    }
}
