package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.repository;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Book;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ITBookStoreApi {

    String API_URL = "https://api.itbook.store/1.0/";

    @GET("new")
    Call<BookList> getBookList();

    @GET("books/{isbn13}")
    Call<Book> getBookDetails(@Path("isbn13") String isbn13);

}
