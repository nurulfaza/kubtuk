package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.callback.BookClickCallback;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.repository.BookList;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.R;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.databinding.BookItemBinding;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Book;

public class BookListAdapter extends RecyclerView.Adapter<BookListAdapter.BookViewHolder> {
    private List<Book> books = new ArrayList<>();

    @Nullable
    private final BookClickCallback bookClickCallback;

    public BookListAdapter(@Nullable BookClickCallback bookClickCallback) {
        this.bookClickCallback = bookClickCallback;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BookItemBinding bookItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.book_item, parent, false);
        bookItemBinding.setCallback(bookClickCallback);
        return new BookViewHolder(bookItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        Book currentBook = books.get(position);
        holder.bookItemBinding.setBook(currentBook);
        //holder.bookItemBinding.executePendingBindings();

    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public void setBooks(BookList booklist) {
        this.books = booklist.getBooks();
        notifyDataSetChanged();
    }

    class BookViewHolder extends RecyclerView.ViewHolder {
        private BookItemBinding bookItemBinding;


        public BookViewHolder(@NonNull BookItemBinding bookItemBinding) {
            super(bookItemBinding.getRoot());
            this.bookItemBinding = bookItemBinding;
        }
    }
}
