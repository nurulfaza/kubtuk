package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.text.DateFormat;
import java.util.Date;

@Entity(tableName = "borrow_table")
public class Borrow {
    //private static Calendar calendar = Calendar.getInstance();

    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private String bookTitle;
    @NonNull
    private String userName;
    private String dateBorrow;

    public Borrow(String bookTitle, String userName) {
        this.bookTitle = bookTitle;
        this.userName = userName;
        this.dateBorrow = DateFormat.getDateInstance().format(new Date());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(@NonNull String bookTitle) {
        this.bookTitle = bookTitle;
    }

    @NonNull
    public String getUserName() {
        return userName;
    }

    public void setUserName(@NonNull String userName) {
        this.userName = userName;
    }

    public String getDateBorrow() {
        return dateBorrow;
    }

    public void setDateBorrow(String dateBorrow) {
        this.dateBorrow = dateBorrow;
    }

}
