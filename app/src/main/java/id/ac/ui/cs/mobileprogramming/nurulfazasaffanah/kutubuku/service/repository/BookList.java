package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.repository;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Book;

public class BookList {
    @SerializedName("total")
    @Expose
    private int total;
    @SerializedName("error")
    @Expose
    private int error;
    @SerializedName("books")
    @Expose
    private List<Book> books = null;

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
