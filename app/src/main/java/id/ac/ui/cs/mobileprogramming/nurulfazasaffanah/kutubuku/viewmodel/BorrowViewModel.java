package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.AppRoomDatabase;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Borrow;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.dao.BorrowDao;

public class BorrowViewModel extends AndroidViewModel {
    private BorrowDao borrowDao;
    private LiveData<List<Borrow>> borrows;

    public BorrowViewModel(@NonNull Application application) {
        super(application);
        borrowDao = AppRoomDatabase.getDatabase(application).borrowDao();
        borrows = borrowDao.getAllBorrows();
    }

    public LiveData<List<Borrow>> getBorrowList() {
        return borrows;
    }

    public void insert(Borrow borrow) {
        borrowDao.insert(borrow);
    }

}
