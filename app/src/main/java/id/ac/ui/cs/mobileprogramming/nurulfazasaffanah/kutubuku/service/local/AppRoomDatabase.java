package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.dao.BookDao;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Book;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Borrow;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.dao.BorrowDao;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.User;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.dao.UserDao;

@Database(entities = {User.class, Borrow.class, Book.class}, version = 2, exportSchema = false)
public abstract class AppRoomDatabase extends RoomDatabase {
    public abstract BorrowDao borrowDao();
    public abstract UserDao userDao();
    public abstract BookDao bookDao();

    private static AppRoomDatabase INSTANCE;

    public static AppRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppRoomDatabase.class, "app_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
