package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.R;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.databinding.BorrowItemBinding;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Borrow;

public class BorrowListAdapter extends RecyclerView.Adapter<BorrowListAdapter.BorrowViewHolder> {
    private List<Borrow> borrows = new ArrayList<>();

    @NonNull
    @Override
    public BorrowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BorrowItemBinding borrowItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.borrow_item, parent, false);
        return new BorrowViewHolder(borrowItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull BorrowViewHolder holder, int position) {
        Borrow currentBorrow = borrows.get(position);
        holder.borrowItemBinding.setBorrow(currentBorrow);

    }

    @Override
    public int getItemCount() {
        return borrows.size();
    }

    public void setBorrows(List<Borrow> borrowList) {
        this.borrows = borrowList;
        notifyDataSetChanged();
    }

    class BorrowViewHolder extends RecyclerView.ViewHolder {
        private BorrowItemBinding borrowItemBinding;


        public BorrowViewHolder(@NonNull BorrowItemBinding borrowItemBinding) {
            super(borrowItemBinding.getRoot());
            this.borrowItemBinding = borrowItemBinding;
        }
    }
}
