package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.R;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Book;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui.BookDetailsFragment;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui.BookListFragment;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui.BorrowListFragment;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui.ConnectionFragment;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui.FillDataFragment;

import static android.net.ConnectivityManager.CONNECTIVITY_ACTION;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ConnectivityReceiver cr = new ConnectivityReceiver();
        IntentFilter filter = new IntentFilter(CONNECTIVITY_ACTION);
        getApplicationContext().registerReceiver(cr, filter);

        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            BookListFragment fragment = new BookListFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragment, BookListFragment.TAG).commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.new_appbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_openGL) {
            showOpenGL();
        }
        return super.onOptionsItemSelected(item);
    }

    public void showDetails(Book book) {
        BookDetailsFragment bookDetailsFragment = BookDetailsFragment.forBook(book.getIsbn13());
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("book")
                .replace(R.id.fragment_container, bookDetailsFragment, null).commit();
    }

    public void showFillData(Book book) {
        FillDataFragment fillDataFragment = FillDataFragment.forBook(book);
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("bookDetail")
                .replace(R.id.fragment_container, fillDataFragment, null).commit();
    }

    public void showBorrowList() {
        BorrowListFragment borrowListFragment = BorrowListFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("fillData")
                .replace(R.id.fragment_container, borrowListFragment, null).commit();
    }

    public void showConnectionAlert() {
        ConnectionFragment connectionFragment = ConnectionFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("previous")
                .replace(R.id.fragment_container, connectionFragment, "CONNECTION_FRAGMENT").commit();
    }

    public void showAccessDenied() {
        AccessDeniedFragment accessDeniedFragment = AccessDeniedFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("previous")
                .replace(R.id.fragment_container, accessDeniedFragment, "PERMISSION_FRAGMENT").commit();
    }

    public void showOpenGL() {
        OpenGLFragment openGLFragment = OpenGLFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("previous")
                .replace(R.id.fragment_container, openGLFragment, "OPENGL_FRAGMENT").commit();
    }

    public class ConnectivityReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
            if (activeNetwork == null) {
                showConnectionAlert();
            }
            else {
                if(getSupportFragmentManager().findFragmentByTag("CONNECTION_FRAGMENT")!= null) {
                    getSupportFragmentManager().popBackStack();
                }
            }

        }
    }

}


