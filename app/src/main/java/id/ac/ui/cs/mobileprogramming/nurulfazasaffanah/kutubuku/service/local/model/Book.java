package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model;

import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.bumptech.glide.Glide;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "book_table")
public class Book {
    @PrimaryKey
    @NonNull
    @SerializedName("isbn13")
    private String isbn13;

    @SerializedName("title")
    private String title;
    @SerializedName("subtitle")
    private String subtitle;
    @SerializedName("price")
    private String price;
    @SerializedName("image")
    private String image;
    @SerializedName("url")
    private String url;
    @SerializedName("authors")
    private String authors;
    @SerializedName("publisher")
    private String publisher;
    @SerializedName("year")
    private String year;
    @SerializedName("desc")
    private String desc;

    public Book(String isbn13, String title, String image) {
        this.isbn13 = isbn13;
        this.title = title;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getIsbn13() {
        return isbn13;
    }

    public String getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public String getUrl() {
        return url;
    }

    public String getAuthors() {
        return authors;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getYear() {
        return year;
    }

    public String getDesc() {
        return desc;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setIsbn13(String isbn13) {
        this.isbn13 = isbn13;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @BindingAdapter({ "image" })
    public static void loadImage(ImageView imageView, String image) {
        Glide.with(imageView.getContext())
                .load(image)
                .into(imageView);
    }

}
