package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.repository.AppRepository;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Book;

public class BookDetailsViewModel extends AndroidViewModel {
    // TODO: Implement the ViewModel
    private final LiveData<Book> bookObservable;
    private final String isbn13;

    public ObservableField<Book> book = new ObservableField<>();

    public BookDetailsViewModel(Application application, String isbn13) {
        super(application);
        this.isbn13 = isbn13;
        bookObservable = AppRepository.getInstance().getBookDetails(isbn13);
    }

    public LiveData<Book> getBookObservable() {
        return bookObservable;
    }

    public void setBook(Book book) {
        this.book.set(book);
    }

    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        @NonNull
        private final Application application;
        private final String isbn13;

        public Factory(@NonNull Application application, String isbn13) {
            this.application = application;
            this.isbn13 = isbn13;
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            if (modelClass.isAssignableFrom(BookDetailsViewModel.class)) {
                return (T) new BookDetailsViewModel(application, isbn13);
            }
            throw new IllegalArgumentException("Unknown ViewModel class ");
        }
    }
}

