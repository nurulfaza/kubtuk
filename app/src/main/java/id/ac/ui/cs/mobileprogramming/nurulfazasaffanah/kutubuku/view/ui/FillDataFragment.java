package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.view.ui;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.R;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Book;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.Borrow;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.User;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.viewmodel.BookViewModel;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.viewmodel.BorrowViewModel;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.viewmodel.UserViewModel;

import static android.app.Activity.RESULT_OK;

public class FillDataFragment extends Fragment implements View.OnClickListener {

    private static final String BOOK = "book";
    private static final int RESULT_PICK_CONTACT =1;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private EditText name;
    private EditText phone;
    private TextView time;
    private Button addContact;
    private Book book;
    private CountDownTimer timer;
    private static UserViewModel userViewModel;
    private static BorrowViewModel borrowViewModel;
    private static BookViewModel bookViewModel;

    public static FillDataFragment newInstance() {
        return new FillDataFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fill_data_fragment, container, false);

        name = view.findViewById(R.id.nameEditText);
        phone = view.findViewById(R.id.phoneEditText);

        time = view.findViewById(R.id.countdown_timer);
        timer = new CountDownTimer(90000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                time.setText("Time remaining: " + millisUntilFinished / 1000 + "s");
            }

            @Override
            public void onFinish() {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Time's Up!");

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }.start();

        addContact = view.findViewById(R.id.addContact);
        addContact.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                //tanya permission
                if (ContextCompat.checkSelfPermission(getContext(),
                        Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(
                            new String[]{Manifest.permission.READ_CONTACTS},
                            PERMISSION_REQUEST_CODE);
                }
                else {
                    Intent in = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                    startActivityForResult(in, RESULT_PICK_CONTACT);
                }
            }
        });

        Button submitButton = view.findViewById(R.id.submitButton);
        submitButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent in = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(in, RESULT_PICK_CONTACT);
            }
            else {
                if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                    ((MainActivity)getActivity()).showAccessDenied();
                }
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        borrowViewModel = ViewModelProviders.of(this).get(BorrowViewModel.class);
        bookViewModel = ViewModelProviders.of(this).get(BookViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,  Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK)
        {
            switch(requestCode){
                case RESULT_PICK_CONTACT:
                    contactPicked(data);
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.submitButton):
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Are you sure you want to borrow this book?");
                builder.setMessage("Make sure you enter correct information.");

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        timer.cancel();
                        AsyncTask.execute(new Runnable() {
                            @Override
                            public void run() {
                                User user = new User(name.getText().toString(), phone.getText().toString());
                                userViewModel.insert(user);

                                Book newBook = new Book(book.getIsbn13(), book.getTitle(), book.getImage());
                                bookViewModel.insert(newBook);

                                Borrow borrow = new Borrow(newBook.getTitle(), user.getName());
                                borrowViewModel.insert(borrow);
                                if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                                    ((MainActivity)getActivity()).showBorrowList();
                                }
                            }

                        });
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
        }
    }

    public void contactPicked(Intent data) {
        try {
            Uri uri = data.getData ();
            Cursor cursor = getActivity().getContentResolver()
                    .query(uri, null, null,null,null);
            cursor.moveToFirst();
            int phoneIndexName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

            String phoneName = cursor.getString(phoneIndexName);
            String phoneNo = cursor.getString(phoneIndex);

            name.setText(phoneName);
            phone.setText(phoneNo);


        } catch (Exception e) {
            e.printStackTrace ();
        }
    }

    public static FillDataFragment forBook(Book book) {
        FillDataFragment fragment = new FillDataFragment();
        fragment.book = book;
        return fragment;
    }


}
