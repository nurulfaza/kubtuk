package id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.AppRoomDatabase;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.model.User;
import id.ac.ui.cs.mobileprogramming.nurulfazasaffanah.kutubuku.service.local.dao.UserDao;

public class UserViewModel extends AndroidViewModel {
    UserDao userDao;

    public UserViewModel(@NonNull Application application) {
        super(application);
        userDao = AppRoomDatabase.getDatabase(application).userDao();
    }

    public void insert(User user) {
        userDao.insert(user);
    }
}
